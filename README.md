# Kim Aleksander Website

This site was built using:

1.  [Default Gatsby starter](https://github.com/gatsbyjs/gatsby-starter-default)
2.  [Emotion](https://emotion.sh)
3.  [React](https://reactjs.org)
4.  [Styled System](https://github.com/jxnblk/styled-system)

Navbar effects by [react-collapse](https://github.com/nkbt/react-collapse)
Responsive background images by [gulp-responsive](https://github.com/mahnunchik/gulp-responsive)
Scroll effects by [Scrollchor](https://github.com/bySabi/react-scrollchor)
Swiper effects by [react-id-swiper](https://github.com/kidjp85/react-id-swiper)
Toasts by [toastify-js](https://github.com/apvarun/toastify-js)
