import gulp from "gulp";
import responsive from "gulp-responsive";

gulp.task("build:responsive-images", () => {
  return gulp
    .src("src/images/space_surfer.jpg")
    .pipe(
      responsive(
        {
          "*.jpg": [
            {
              width: 1440,
              rename: { suffix: "-xl-1x" },
            },
            {
              width: 1440,
              rename: {
                suffix: "-xl-1x",
                extname: ".webp",
              },
            },
            {
              width: 1980,
              rename: { suffix: "-xl-2x" },
            },
            {
              width: 1980,
              rename: {
                suffix: "-xl-2x",
                extname: ".webp",
              },
            },
            {
              width: 1024,
              rename: { suffix: "-lg-1x" },
            },
            {
              width: 1024,
              rename: {
                suffix: "-lg-1x",
                extname: ".webp",
              },
            },
            {
              width: 1980,
              rename: { suffix: "-lg-2x" },
            },
            {
              width: 1980,
              rename: {
                suffix: "-lg-2x",
                extname: ".webp",
              },
            },
            {
              width: 768,
              rename: { suffix: "-md-1x" },
            },
            {
              width: 768,
              rename: {
                suffix: "-md-1x",
                extname: ".webp",
              },
            },
            {
              width: 768 * 2,
              rename: { suffix: "-md-2x" },
            },
            {
              width: 768 * 2,
              rename: {
                suffix: "-md-2x",
                extname: ".webp",
              },
            },
            {
              width: 540,
              rename: { suffix: "-sm-1x" },
            },
            {
              width: 540,
              rename: {
                suffix: "-sm-1x",
                extname: ".webp",
              },
            },
            {
              width: 540 * 2,
              rename: { suffix: "-sm-2x" },
            },
            {
              width: 540 * 2,
              rename: {
                suffix: "-sm-2x",
                extname: ".webp",
              },
            },
          ],
        },
        {
          crop: "centre",
          quality: 70,
          responsive: true,
          withMetadata: false,
          withoutEnlargement: false,
        }
      )
    )
    .pipe(gulp.dest("src/images/responsive"));
});
