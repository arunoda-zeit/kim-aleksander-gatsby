import styled from "@emotion/styled";
import { fontSize, space, width } from "styled-system";

export default styled.div`
  ${fontSize};
  ${space};
  ${width};
  align-items: center;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  min-height: 100vh;
  overflow: hidden;
  text-align: center;
`;
