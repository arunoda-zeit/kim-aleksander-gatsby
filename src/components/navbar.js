import React from "react";
import { Link } from "gatsby";
import { css } from "emotion";
import Collapse from "react-collapse";
import PropTypes from "prop-types";
import Scrollchor from "../utils/scrollchor-item";
import "../utils/fonts/fa-solid";
import "../utils/fonts/fontawesome";
import "typeface-exo-2";

const navContainer = css`
  align-items: center;
  background-color: rgba(0, 0, 0, 0.2);
  color: rgba(255, 255, 255, 0.7);
  display: flex;
  flex-direction: row nowrap;
  font-family: "Exo 2", sans-serif;
  height: 4em;
  justify-content: space-around;
  overflow: hidden;
  position: absolute;
  text-decoration: none;
  top: 0;
  width: 100%;
  @media (max-width: 36em) {
    justify-content: space-between;
    padding: 0 2rem;
  }
`;
const navMenu = css`
  align-items: center;
  background-color: rgba(0, 0, 0, 0.2);
  color: rgba(255, 255, 255, 0.7);
  display: none;
  flex-flow: row nowrap;
  font-family: "Exo 2", sans-serif;
  justify-content: center;
  overflow: hidden;
  position: absolute;
  text-align: center;
  text-decoration: none;
  top: 4em;
  width: 100%;
  @media (max-width: 36em) {
    display: flex;
  }
`;

const navLink = css`
  padding: 1em;
  @media (max-width: 36em) {
    display: none;
  }
`;

const hamburger = css`
  background-color: transparent;
  border: none;
  color: rgba(255, 255, 255, 0.7);
  font-size: 1.25rem;
  display: inline;
  &:focus {
    outline: none;
  }
  @media (min-width: 36em) {
    display: none;
  }
`;

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOpened: this.props.isOpened };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isOpened: !prevState.isOpened,
    }));
  }
  render() {
    const { isOpened } = this.state;
    return (
      <nav style={{ display: "flex", flexDirection: "column", width: "100%" }}>
        <div>
          <div className={navContainer}>
            <div>
              <Link to="/">Kim Aleksander</Link>
            </div>
            <div>
              <button className={hamburger} onClick={this.handleClick}>
                <i className="fas fa-bars" />
              </button>
              <Scrollchor to="get-fp">
                <a className={navLink}>False Positives</a>
              </Scrollchor>
              <Scrollchor to="#ss-signup">
                <a className={navLink}>Soul Surfer</a>
              </Scrollchor>
              <Scrollchor to="#connect">
                <a className={navLink}>Connect</a>
              </Scrollchor>
            </div>
          </div>
          <Collapse isOpened={isOpened} className={navMenu}>
            <ul style={{ listStyleType: "none", padding: "1rem", margin: "0" }}>
              <li style={{ padding: ".75rem" }}>
                <Scrollchor to="get-fp">
                  <a onClick={this.handleClick}>False Positives</a>
                </Scrollchor>
              </li>
              <li style={{ padding: ".75rem" }}>
                <Scrollchor to="#ss-signup">
                  <a onClick={this.handleClick}>Soul Surfer</a>
                </Scrollchor>
              </li>
              <li style={{ padding: ".75rem" }}>
                <Scrollchor to="#connect">
                  <a onClick={this.handleClick}>Connect</a>
                </Scrollchor>
              </li>
            </ul>
          </Collapse>
        </div>
      </nav>
    );
  }
}
export default Navbar;

Navbar.defaultProps = {
  isOpened: false,
};
Navbar.propTypes = {
  isOpened: PropTypes.bool,
};
