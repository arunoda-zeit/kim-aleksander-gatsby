import React, { Component } from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";
import { injectGlobal } from "emotion";
import { ThemeProvider } from "emotion-theming";
import "prismjs/themes/prism-tomorrow.css";
import "normalize.css";
import "typeface-exo-2";
import { StaticQuery, graphql } from "gatsby";
import $ from "jquery";

const theme = {
  breakpoints: ["36em", "48em", "62em", "75em"],
  colors: { soul: "#a350b5", sea: "#62b550" },
  fontSizes: [
    "0.75rem",
    "0.875rem",
    "1rem",
    "1.25rem",
    "1.5rem",
    "2rem",
    "3rem",
    "4rem",
    "5rem",
  ],
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
};

injectGlobal`
  * {
    font-family: {
      apple-system,
      BlinkMacSystemFont,
      Seguoe UI,
      Roboto,
      Helvetical Neue,
      Arial,
      sans-serif;
    }
    box-sizing: border-box;
  };
  body {
    background-color: black;
    display: none;
    margin: 0;
  };
  p {
    line-height: 1.4;
  };
  a {
    text-decoration: none;
    color: inherit;
  };
  a:hover {
    cursor: pointer;
  };
`;

class Layout extends Component {
  componentDidMount() {
    $(document).ready(() => {
      $("body").css("display", "none");
      $("body").fadeIn(4000);
    });
  }
  render() {
    const { children } = this.props;
    return (
      <div>
        <StaticQuery
          query={graphql`
            query SiteTitleQuery {
              site {
                siteMetadata {
                  title
                }
              }
            }
          `}
          render={data => (
            <>
              <Helmet
                title={data.site.siteMetadata.title}
                meta={[
                  {
                    name: "description",
                    content:
                      "Kim Aleksander Thriller Writer of False Positives and Soul Surfer",
                  },
                  {
                    name: "og:description",
                    content:
                      "Kim Aleksander Thriller Writer of False Positives and Soul Surfer",
                  },
                  {
                    name: "keywords",
                    content:
                      "Thriller, Amazon, Best Seller, Techno Thriller, Conspiracy, Science Fiction",
                  },
                  { name: "og:title", content: "Kim Aleksander" },
                  { name: "og:site_name", content: "Kim Aleksander" },
                  { name: "og:url", content: "http://kimaleksander.com" },
                  {
                    name: "google-site-verification",
                    content: "uubLfpcORmDWkM8pI-yKODVVZD3VSpUYFSwkQJcOuZE",
                  },
                ]}
              />
              <ThemeProvider theme={theme}>
                <div>{children}</div>
              </ThemeProvider>
            </>
          )}
        />
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.array,
  data: PropTypes.object,
};

export default Layout;
