import styled from "@emotion/styled";
import { fontSize, space } from "styled-system";

export default styled.p`
  ${fontSize};
  ${space};
  color: white;
  grid-area: title;
  font-family: "Exo 2", sans-serif;
  font-weight: 300;
  letter-spacing: 1px;
  text-align: center;
  text-shadow: 2px 2px black;
`;
