import styled from "@emotion/styled";
import { fontSize, space } from "styled-system";
import "typeface-russo-one";

export default styled.h1`
  ${fontSize};
  ${space};
  color: white;
  grid-area: title;
  font-family: "Russo One", sans-serif;
  letter-spacing: 1px;
  text-align: center;
  text-shadow: 2px 2px black;
  text-transform: uppercase;
`;
