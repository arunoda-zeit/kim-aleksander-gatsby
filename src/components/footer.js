import React from "react";
import styled from "@emotion/styled";
import "../utils/fonts/fa-brands";
import "../utils/fonts/fa-solid";
import "../utils/fonts/fontawesome";
import "typeface-exo-2";

const Container = styled.div`
  align-items: center;
  background-color: rgba(0, 0, 0, 0.2);
  bottom: 0;
  color: rgba(255, 255, 255, 0.7);
  display: flex;
  flex-direction: row nowrap;
  font-family: "Exo 2", sans-serif;
  height: 4em;
  justify-content: space-between;
  overflow: hidden;
  padding: 0 1rem;
  position: absolute;
  text-decoration: none;
  width: 100%;
  @media (min-width: 576px) {
    padding: 0 1rem;
  }
  @media (min-width: 768px) {
    justify-content: space-around;
  }
`;

class Footer extends React.Component {
  render() {
    return (
      <div>
        <Container>
          <div>
            <p>&copy; Kim Aleksander</p>
          </div>
          <div>
            <a
              style={{ fontSize: "1.33rem" }}
              href="https://www.facebook.com/AuthorKimAleksander/"
              title="Facebook"
            >
              <span className="fa-layers fa-fw">
                <i
                  className="fa fa-square fa-inverse"
                  data-fa-transform="shrink-1"
                />
                <i className="fab fa-facebook" style={{ color: "#3b5998" }} />
              </span>
            </a>
            <a
              href="https://www.goodreads.com/author/show/5416967.Kim_Aleksander"
              style={{ fontSize: "1.33rem", paddingLeft: ".5rem" }}
              title="GoodReads"
            >
              <span className="fa-layers fa-fw">
                <i
                  className="fa fa-square"
                  style={{ color: "#553b08" }}
                  data-fa-transform="shrink-1"
                />
                <i className="fab fa-goodreads" style={{ color: "#edead7" }} />
              </span>
            </a>
            <a
              style={{
                color: "#1da1f2",
                fontSize: "1.33rem",
                paddingLeft: ".5rem",
              }}
              href="https://twitter.com/KimAleksander"
              title="Twitter"
            >
              <i className="fab fa-twitter" />
            </a>
          </div>
        </Container>
      </div>
    );
  }
}
export default Footer;
