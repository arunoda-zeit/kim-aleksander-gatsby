import styled from "@emotion/styled";
import { fontSize, space, width } from "styled-system";

export default styled.div`
  ${fontSize};
  ${space};
  ${width};
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
  overflow: hidden;
`;
