import styled from "@emotion/styled";
import { space, width } from "styled-system";

export default styled.div`
  ${space};
  ${width};
  align-items: center;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  min-height: 100vh;
`;
