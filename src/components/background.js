import styled from "@emotion/styled";

export default styled.div`
  background-position: left bottom;
  background-repeat: no-repeat;
  background-size: cover;
  min-height: 100vh;
  position: relative;
`;
