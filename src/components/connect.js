import React, { Component } from "react";
import styled from "@emotion/styled";
import Button from "./button";
import Flex from "./flex_column";
import Flex100 from "./flex_column_100vh";
import Input from "./input";
import Recaptcha from "react-google-recaptcha";
import Title from "./title";
import Toastify from "toastify-js";
import validator from "validator";
import "../utils/fonts/fa-solid";
import "../utils/fonts/fontawesome";
import "../utils/toastify.css";

const TextArea = styled.textarea`
  background-color: rgba(0, 0, 0, 0.6);
  border: none;
  border-radius: 0.35rem;
  color: white;
  font-family: "Exo 2", sans-serif;
  padding: 1rem;
  &:focus {
    outline: none;
  }
  &:invalid {
    box-shadow: none;
  }
`;

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
};

class Connect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      url: "",
      subject: "",
      message: "",
      info: "",
    };

    this.baseState = this.state;
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRecapthca = this.handleRecaptcha.bind(this);
  }

  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  badEmail = () =>
    Toastify({
      text: "Please fix your email address.",
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "error",
    }).showToast();

  badUrl = () =>
    Toastify({
      text: "Please fix your web address.",
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "error",
    }).showToast();

  badSend = error =>
    Toastify({
      text: "Oops!  Something went wrong. " + error,
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "error",
    }).showToast();

  goodSend = () =>
    Toastify({
      text: "Message Sent",
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "success",
    }).showToast();

  handleRecaptcha = value => {
    this.setState({ "g-recaptcha-response": value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (!validator.isEmail(this.state.email)) {
      this.badEmail();
      return;
    }

    if (!validator.isURL(this.state.url)) {
      this.badUrl();
      return;
    }

    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "connect", ...this.state }),
    })
      .then(() => {
        this.setState(this.baseState);
      })
      .then(() => {
        this.goodSend();
      })
      .catch(error => this.badSend(error));
  };

  render() {
    return (
      <Flex100 py={4}>
        <Title fontSize={[5, 6, 6, 7]} mb={[3, 4]}>
          Connect
        </Title>
        <form
          autoComplete="off"
          data-netlify="true"
          data-netlify-honeypot="info"
          data-netlify-recaptcha="true"
          name="connect"
          onSubmit={this.handleSubmit}
        >
          <Flex mb={"4em"}>
            <Flex width={[90 / 100]}>
              <input type="hidden" name="connect" value="contact" />
              <input
                onChange={this.handleInputChange}
                type="hidden"
                name="info"
                value={this.state.info}
              />
              <Input
                icon="fas fa-user-circle"
                name="username"
                onChange={this.handleInputChange}
                placeholder="Andrew Milas"
                type="text"
                value={this.state.username}
              />
              <Input
                icon="fas fa-envelope"
                name="email"
                onChange={this.handleInputChange}
                placeholder="andy@geronimo-bar.co.th"
                type="email"
                value={this.state.email}
              />
              <Input
                icon="fas fa-globe"
                name="url"
                onChange={this.handleInputChange}
                placeholder="https://geronimo-bar.co.th"
                type="url"
                value={this.state.url}
              />
              <Input
                icon="fas fa-comment-alt"
                name="subject"
                onChange={this.handleInputChange}
                placeholder="Subject"
                type="text"
                value={this.state.subject}
              />
              <TextArea
                style={{ width: "100%" }}
                rows="10"
                cols="50"
                name="message"
                onChange={this.handleInputChange}
                placeholder="Your Message..."
                required
                value={this.state.message}
              />
              <br />
              <Recaptcha
                onChange={this.handleRecaptcha}
                ref={el => {
                  this.captcha = el;
                }}
                sitekey="6LdXRWEUAAAAABtMhuLgDfFiL0aFgDL400t6N3YP"
                theme="dark"
              />
              <Button
                type="submit"
                style={{ width: "100%" }}
                fontSize={[2, 2, 3]}
                my={3}
                p={".6em"}
              >
                Send
              </Button>
            </Flex>
          </Flex>
        </form>
      </Flex100>
    );
  }
}

export default Connect;
