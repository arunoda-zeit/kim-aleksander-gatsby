import React from "react";
import styled from "@emotion/styled";
import Button from "./button";
import Input from "../components/input";
import Title from "../components/title";
import Toastify from "toastify-js";
import validator from "validator";
import "../utils/fonts/fa-solid";
import "../utils/fonts/fontawesome";
import "../utils/toastify.css";

const Blurb = styled.div`
  align-items: center;
  display: flex;
  flex-flow: row nowrap;
  grid-area: blurb;
`;

const GetNotified = styled.div`
  display: flex;
  flex-flow: column nowrap;
  grid-area: gn;
  justify-content: flex-start;
  text-align: center;
`;

// If I change the below to use template literal, the grid breaks.  Why?!
const Section = styled("div")({
  color: "white",
  gridGap: "0rem 1.5rem",
  display: "grid",
  gridTemplateColumns: "1fr",
  gridTemplateAreas: `
    "title"
    "blurb"
    "gn"
  `,
  width: "90%",
  "@media(min-width: 768px)": {
    gridTemplateColumns: "2fr 1fr",
    gridTemplateAreas: `
      "title title"
      "blurb gn"
    `,
    width: "80%",
  },
});

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
};

class SoulSurferSignup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      information: "",
    };

    this.baseState = this.state;
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  badEmail = () =>
    Toastify({
      text: "Please fix your email address.",
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "error",
    }).showToast();

  badSend = error =>
    Toastify({
      text: "Oops!  Something went wrong. " + error,
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "error",
    }).showToast();

  goodSend = () =>
    Toastify({
      text: "Message Sent",
      backgroundColor: "rgba(0,0,0,0.8)",
      gravity: "bottom",
      duration: 3000,
      classes: "success",
    }).showToast();

  handleSubmit = event => {
    event.preventDefault();
    if (!validator.isEmail(this.state.email)) {
      this.badEmail();
      return;
    }

    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "ss-signup", ...this.state }),
    })
      .then(() => {
        this.setState(this.baseState);
      })
      .then(() => {
        this.goodSend();
      })
      .catch(error => this.badSend(error));
  };

  render() {
    return (
      <Section>
        <Title fontSize={[5, 6, 6, 7]} mb={[4, 5]}>
          soul surfer
        </Title>
        <Blurb>
          <p
            style={{
              fontFamily: "Sanchez, serif",
              fontWeight: "100",
              backgroundColor: "rgba(0,0,0,0.6)",
              borderRadius: "0.75rem",
              marginTop: "0",
              padding: "2rem",
              textAlign: "justify",
            }}
          >
            In the year 2059, an underground hacktivist, JOSEPH PLUMBER,
            develops a “time bomb” technology to send information into the past
            with the hopes to save the world from a dystopian future. When he
            discovers that his time bombs are being used to benefit a select few
            while enslaving the masses, Joe finds himself embroiled in a
            dangerous conspiracy that pervades the highest realms of power.
          </p>
        </Blurb>
        <GetNotified>
          <p
            style={{
              fontFamily: "'Exo 2', sans-serif",
              margin: "0",
              textShadow: "2px 2px black",
            }}
          >
            SOUL SURFER is a work in progress.
          </p>
          <p
            style={{
              fontFamily: "'Exo 2', sans-serif",
              marginTop: "0",
              textShadow: "2px 2px black",
            }}
          >
            Sign up here to get notified when it’s released.
          </p>
          <form
            autoComplete="off"
            data-netlify="true"
            data-netlify-honeypot="information"
            name="ss-signup"
            onSubmit={this.handleSubmit}
            style={{
              display: "flex",
              flexFlow: "column nowrap",
              width: "100%",
            }}
          >
            <input type="hidden" name="ss-signup" value="contact" />
            <input
              onChange={this.handleInputChange}
              type="hidden"
              name="information"
              value={this.state.information}
            />
            <Input
              icon="fas fa-user-circle"
              name="username"
              onChange={this.handleInputChange}
              placeholder="Marnie McCloud"
              type="text"
              value={this.state.username}
            />
            <Input
              icon="fas fa-envelope"
              name="email"
              onChange={this.handleInputChange}
              placeholder="mmcloud@burns-lynch.com"
              type="email"
              value={this.state.email}
            />
            <Button type="submit" fontSize={2} mt={0} px={3} py={12}>
              Get Notified
            </Button>
          </form>
        </GetNotified>
      </Section>
    );
  }
}
export default SoulSurferSignup;
