import styled from "@emotion/styled";
import { fontSize, space } from "styled-system";
import "typeface-exo-2";

export default styled.a`
  ${fontSize};
  ${space};
  background-color: rgba(0, 0, 0, 0.4);
  border-radius: 0.33em;
  border-style: solid;
  border-width: 1px;
  color: white;
  font-family: "Exo 2", sans-serif;
  font-weight: 400;
  letter-spacing: 1px;
  opacity: 1;
  text-decoration: none;
  transition: background-color 0.15s ease-in;
  text-transform: uppercase;
  &:hover {
    background-color: rgba(255, 255, 255, 0.1);
  }
`;
