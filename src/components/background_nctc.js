import styled from "@emotion/styled";
import bgSmall from "../images/nctc_dark-sm-1x.jpg";
import bgSmallWebp from "../images/nctc_dark-sm-1x.webp";
import bgSmallRetina from "../images/nctc_dark-sm-2x.jpg";
import bgSmallRetinaWebp from "../images/nctc_dark-sm-2x.webp";
import bgMedium from "../images/nctc_dark-md-1x.jpg";
import bgMediumWebp from "../images/nctc_dark-md-1x.webp";
import bgMediumRetina from "../images/nctc_dark-md-2x.jpg";
import bgMediumRetinaWebp from "../images/nctc_dark-md-2x.webp";
import bgLarge from "../images/nctc_dark-lg-1x.jpg";
import bgLargeWebp from "../images/nctc_dark-lg-1x.webp";
import bgLargeRetina from "../images/nctc_dark-lg-2x.jpg";
import bgLargeRetinaWebp from "../images/nctc_dark-lg-2x.webp";
import bgXLarge from "../images/nctc_dark-xl-1x.jpg";
import bgXLargeWebp from "../images/nctc_dark-xl-1x.webp";
import bgXLargeRetina from "../images/nctc_dark-xl-2x.jpg";
import bgXLargeRetinaWebp from "../images/nctc_dark-xl-2x.webp";

export default styled.div`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  min-height: 100vh;
  position: relative;

  background-image: url("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QPERXhpZgAASUkqAAgAAAACADIBAgAUAAAAJgAAAGmHBAABAAAAOgAAAEAAAAAyMDE4OjA2OjE4IDE5OjIwOjMzAAAAAAAAAAMAAwEEAAEAAAAGAAAAAQIEAAEAAABqAAAAAgIEAAEAAABSAwAAAAAAAP/Y/+AAEEpGSUYAAQEAAAEAAQAA/9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8AAEQgAFgAbAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A860fQ7OTToZZQZmkXJBY7Rk/zrH1LTBHIzW4YRknCMM5AxnkdPxqbTL5rS3mtwWNvKcq3Qoe4robf/SobeCFphFLIRI4cttwBguxP3fb24rmtGS2Ld7nFw+dBO8agBwufKk+6x/+tVWSO1eRmeKZWJ5AIIB/HmuzGlyjTneaMSRoxZmVwdp9R7A9/rVOG03RKY7VpFxgMY+tRytAlfcwjuhmdWw394eoqwl1PAyqkhMbANtPvRRSZTL13rk9zoyWLgLGjjO3gsOwJ+vNUY9ZvxGoEiYAwMr2/Oiihydyb22P/9n/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wgARCAAWABsDASIAAhEBAxEB/8QAGAAAAwEBAAAAAAAAAAAAAAAAAAIDBAH/xAAWAQEBAQAAAAAAAAAAAAAAAAAAAQL/2gAMAwEAAhADEAAAAcUu3yyrqSJdBXQE/8QAGxAAAgIDAQAAAAAAAAAAAAAAAAECERASITH/2gAIAQEAAQUCjBU444J0emvKxY53HZn/xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAEDAQE/AR//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAECAQE/AR//xAAXEAEBAQEAAAAAAAAAAAAAAAAQESAx/9oACAEBAAY/AsR4w//EABsQAQADAAMBAAAAAAAAAAAAAAEAESExQVGh/9oACAEBAAE/IUFduVOQsa+RPDKz0YYAul2W5kMZ8ThljuJ44e8//9oADAMBAAIAAwAAABBld7//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAEDAQE/EB//xAAYEQADAQEAAAAAAAAAAAAAAAAAAREQIf/aAAgBAgEBPxDjRHlP/8QAHRABAAMAAwADAAAAAAAAAAAAAQARITFBUXGR8P/aAAgBAQABPxDagPchI3Dof2SzBtcuGIVQfBjWj1Pk27kF3XyvULKCDaiNS0WJ68prDvsFgKnagRAB2u5QMfU//9k=");

  @media (min-width: 0px) {
    background-image: url(${bgSmall});
    background-image: -webkit-image-set(
      url(${bgSmallWebp}) 1x,
      url(${bgSmallRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgSmall}) 1x, url(${bgSmallRetina}) 2x);
  }

  @media (min-width: 576px) {
    background-image: url(${bgMedium});
    background-image: -webkit-image-set(
      url(${bgMediumWebp}) 1x,
      url(${bgMediumRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgMedium}) 1x, url(${bgMediumRetina}) 2x);
  }

  @media (min-width: 1024px) {
    background-image: url(${bgLarge});
    background-image: -webkit-image-set(
      url(${bgLargeWebp}) 1x,
      url(${bgLargeRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgLarge}) 1x, url(${bgLargeRetina}) 2x);
  }

  @media (min-width: 1440px) {
    background-image: url(${bgXLarge});
    background-image: -webkit-image-set(
      url(${bgXLargeWebp}) 1x,
      url(${bgXLargeRetinaWebp}) 2x
    );
    background-image: image-set(url(${bgXLarge}) 1x, url(${bgXLargeRetina}) 2x);
  }
`;
