import React from "react";
import PropTypes from "prop-types";
import Anchor from "../components/anchor_button";
import BackgroundFP from "../components/background_nctc";
import BackgroundSS from "../components/background_ss";
import Connect from "../components/connect";
import Flex from "../components/flex";
import Footer from "../components/footer";
import GetFalsePositives from "../components/getfalsepositives";
import Hero from "../components/hero";
import Navbar from "../components/navbar";
import Reviews from "../components/reviews";
import Scrollchor from "../utils/scrollchor-item";
import SoulSurferSignup from "../components/soulsurfersignup";
import Title from "../components/title";
import Layout from "../components/layout";

const IndexPage = () => (
  <Layout>
    <BackgroundSS>
      <Hero>
        <Navbar />
        <Title fontSize={[5, 6, 6, 7, 7]} mb={[3, 3]} px={3}>
          Kim Aleksander
        </Title>
        <Anchor fontSize={[2, 2, 3]} mt={[1, 3]} p={".6em"}>
          <Scrollchor to="#ss-signup" animate={{ offset: 0, duration: 500 }}>
            soul surfer
          </Scrollchor>
        </Anchor>
      </Hero>
    </BackgroundSS>
    <BackgroundFP>
      <Reviews />
    </BackgroundFP>
    <BackgroundSS id="ss-signup">
      <Flex py={4}>
        <SoulSurferSignup />
      </Flex>
    </BackgroundSS>
    <BackgroundFP id="get-fp">
      <Flex py={4}>
        <GetFalsePositives />
      </Flex>
    </BackgroundFP>
    <BackgroundSS id="connect">
      <Connect />
      <Footer />
    </BackgroundSS>
  </Layout>
);

export default IndexPage;
