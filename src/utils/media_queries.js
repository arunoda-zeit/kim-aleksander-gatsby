import { css } from "emotion";
import facepaint from "facepaint";

const breakpoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  tallPhone: "(max-width: 360px) and (min-height: 740px)",
};

const mq = Object.keys(breakpoints).reduce((accumulator, label) => {
  let prefix = typeof breakpoints[label] === "string" ? "" : "min-width:";
  let suffix = typeof breakpoints[label] === "string" ? "" : "px";
  accumulator[label] = cls =>
    css`
      @media (${prefix + breakpoints[label] + suffix}) {
        ${cls};
      }
    `;
  return accumulator;
}, {});

export default mq;
