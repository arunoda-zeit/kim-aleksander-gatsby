---
path: "/blog/thoughts-on-CSS-in-JS"
date: "2018-06-07"
title: "Thoughts on CSS-in-JS"
---

# Why I'm Writing This

I started writing this first to test my understanding and ability to speak using terms related to CSS-in JS, styled-components, and styled system (mainly) around React.

I got into this about a week ago when I began learning about ways to build things without using things like Bootstrap, Foundation, or Tachyons. Me writing this is not any form of drum-banging for or against CSS libraries, but moreso a way to explore what potential value CSS-in-JS (and the various ways of implementing it) offers.

So for fun, let's start from scratch and see what comes of it.

## Create a Base

### Normalize CSS

Okay, without a CSS library, we're stuck with the user agent stylesheet. So we should probably start off by importing normalize.css into the main layout.

### Inject Globals

Create some basic globals (using [Emotion](https://emotion.sh/docs/inject-global))

> :point_up: I thought about using [Typography](https://kyleamathews.github.io/typography.js/), but elected not to, so as to start as "from scratch" with as few libraries as possible.

```css
injectGlobal`
  * {
    font-family:
    apple-system,
    BlinkMacSystemFont,
    Seguoe UI,
    Roboto,
    Helvetical Neue,
    Arial,
    sans-serif;
    box-sizing: border-box;
  };
  a {
    text-decoration: none;
  };
  a:hover {
    cursor: pointer;
  };
`;
```

## Add a Styled Component

Alright, this is a base to build on, but it's barely enough and not-at-all responsive. Let's use a styled component (via [Emotion](https://emotion.sh/docs/styled), named `Hero`, to illustrate things as I work through this.

> :point_up: Note that the lack of any margins or padding at this point is intentional. Also, we're using [Object Styles](https://emotion.sh/docs/object-styles) here, as it allows us to "nest" things like `h1` and `h3` rather than creating individual components from them.

```js
import styled from "react-emotion";

const Hero = styled.div({
  alignItems: "center",
  backgroundColor: "black",
  display: "flex",
  flexDirection: "column",
  height: "100vh",
  justifyContent: "center",
  textAlign: "center",
  overflow: "hidden",
  h1: {
    color: "white",
    letterSpacing: "2px",
    textShadow: "2px 2px black",
    textTransform: "uppercase",
  },
  h3: {
    color: "white",
    fontWeight: "100",
    textShadow: "2px 2px black",
  },
});

export default Hero;
```

> :question: I like the idea of a styled component (e.g. Hero) having its own elements (e.g. `h1` and `h3`) that can be "styled" within the styled component itself. Is this even a good idea?

Let's look at the `header` elements. At this point, aside from what is defined in the above, `h1` recieves a .67em bottom border from normalize.css. The `h3` has top and bottom margins of 18.72px, which is based on 1.17em, which is due to the user agent stylesheet's font-size for `h3` being 1.17em.

### Forks in the Road

Let's add some padding to the Hero component.

:question: Decision Point: should we do this within the styled component, with "vanilla" CSS-in-JS, or should we introduce somethng like `styled-system`? What are the pro and cons of each method?

#### Adding Padding in the Styled Component

Here we add a single line to the Hero styeled component:

```js
const Hero = styled.div({
  alignItems: "center",
  display: "flex",
  flexDirection: "column",
  height: "100vh",
  justifyContent: "center",
  padding: "0 1rem", // <== New Padding Added
  textAlign: "center",
  overflow: "hidden",
```

Nothing special there. Easy peasy, right?

#### Adding Padding Using "Vanilla" CSS-in-JS

```jsx
import { css } from "react-emotion";
import Hero from "../components/hero";

const IndexPage = () => (
  <Hero
    className={css`
      padding: 0 1rem;
    `}
  >
    <h1>Big Fancy Title</h1>
    <h3>I'm a fancy tagline. Ain't I fancy!</h3>
  </Hero>
);
```

This pretty much does the same thing as adding padding to the styled component, but you can get more granular with it. For example, you can style the `h1` directly should you want to.

> :question: At this point, I don't see a whole lot of benefit going the CSS-in-JS route here, except maybe to tweak an element beyond what wasn't generically styled in the styled component. Any thoughts?

#### Adding Padding using Styled-System

I like to use utility classes, like `px-sm-0 px-md-1` (Bootstrap). These are neat because you can tweak things at a granular level _and_ leverage media queries. Without a CSS Library, these utility classes are missing.

Adding [Styled System](https://jxnblk.com/styled-system/) to the mix allows us to do something similar to utility classes.

> :point_up: There is a **lot** more you can do with `styled system` than just padding. I'm just keeping it simple here.

So, first, we can add `styled system` to the `Hero` component:

```js
import { space } from "styled-system"; // Import styled system

const Hero = styled.div(space, {
  // Add { space } utility to Hero styled component
  alignItems: "center",
  display: "flex",
  flexDirection: "column",
  height: "100vh",
  justifyContent: "center", // Padding line deleted from prior example
  textAlign: "center",
  overflow: "hidden",
  h1: {
    color: "white",
    letterSpacing: "2px",
    textShadow: "2px 2px black",
    textTransform: "uppercase",
  },
  h3: {
    color: "white",
    fontWeight: "100",
    textShadow: "2px 2px black",
  },
});

export default Hero;
```

Doing that will allow us to do this:

```jsx
import Hero from "../components/hero";
const IndexPage = () => (
  <Hero px={[1, 2, 3, 4]}> // <== Look here!
    <h1>Big Fancy Title</h1>
    <h3>I'm a fancy tagline. Ain't I fancy!</h3>
  </Hero>
);
```

> :question: One thing I can't figure out how to do is add a utility to anything other than the Hero component. For example, you can't seem use a utility on the `h1` component.  
> I've got a feeling that this is due to how I've architected things, using a styled component and nesting the `h1` element inside of it.
> My initial thought was that Hero would contain of of the styles for itself, \*_including_ it's "sub-styles", like `h1`, which would remain unique to Hero.
> I guess I _could_ create unique `H1` component and use it in Hero, but is that really worth the bother?

If you're not familiar with what `px={[1, 2, 3, 4]}` does, it's `styled-system` shorthand. Let me try to explain as briefly as I can here:

The `{ space }` utility we imported and added into the Hero styled component above comes with a default set of values defined in a pre-defined array:

```js
const space = [0, 4, 8, 16, 32, 64, 128, 256, 512];
```

`Styled-system` also comes with a default set of `breakpoints` (another array):

```js
const breakpoints = ["40em", "52em", "64em"];
```

These breakpoints serve to create the following media queries:

```css
@media screen and (min-width: 40em){},
@media screen and (min-width: 52em){},
@media screen and (min-width: 64em){},
```

Now, when we use `px={[1, 2, 3, 4]}` as a parameter on Hero it will:

1.  Use `1` to lookup the value of the first index of the `space` array (i.e. 4px) and use it to pad the Hero component on both the left and right with 4px when the viewport is under 40em (640px).
2.  Use `2` to lookup the value of the second index of the `space` array (i.e. 8px) and use it to pad the Hero component on both the left and right with 8px when the viewport is sized from 40em up to 52em (832px).
3.  Use `3` to lookup the value of the third index of the `space` array (i.e. 16px) and use it to pad the Hero component on both the left and right with 16px when the viewport is sized from 52em up to 64em (1024px).
4.  Use `4` to lookup the value of the fourth index of the `space` array (i.e. 32px) and use it to pad the Hero component on both the left and right with 32px when the viewport is sized at 64em upward.

> :point_up: You can define your own values for `space` and `breakpoints` by creating your own theme, but I'm not going to go down that rabbit hole quite yet.

## Initial Findings

So what do we end up with after all that?

1.  The Hero component (and any others) gets some base styling from injectGlobals and normalize.css.
2.  The Hero component has it's own styles defined within itself. This is useful in that it can be reused elsewhere. Style once, use many.
3.  When something needs a bit more granular styling, we can use CSS-in-JS to do that were we render the component.
4.  If we like using utility classes, we can use `styled-system` to do just that.
5.  Styled-system allows us to leverage media queries as well, which gives us respnsiveness.

## Lingering Questions

1.  What are the core benefits of choosing to develop using these techniques over a CSS Library such as Bootstrap, Foundation, or Tachyons?
2.  Does it require more/less effort to build an maintain a solution using these techniques over others?
3.  Do using any of these techniques make one's code any more/less readable?
4.  Are situations where implementing some or all of these techniques delivers more value than others?
