---
path: "/blog/gatsby-picture-for-background-images"
date: "2018-06-07"
title: "Gatsby picture for background images"
---

This is a great thread about a very neat package! I came across it because I was trying to do just what the title says, use Gatsby Image for background images. And @ooloth is spot on about losing the best features when going that route.

Though the thread is closed, I thought I'd add my two cents.

As I see it, Gatsby Image gives you a React component, `<Img />` that you can give props (if you're GraphQL queries are setup right, and you can't hand a CSS background-image a component.

You \*_can_ do this (css) is react-emotion:

```css
className={css`
  background-image: url(${this.props.data.imageName.sizes.src});
`}
```

But that pretty much defeats the entire purpose of what we're trying to accomplish, as it's pulling the original image with none of the Gatsby Image magic happening.

Now, while using absolute positioning is okay for certain use cases, it makes it super hard (if not impossible) to do something like [this](https://kimaleksander.com/). I've tried and tried!

Admittedly, that site is a complete wordpress hack from years ago, but it uses media queries to render different images at different viewport widths, like this:

```css
.ss-hero,
.ss-signup {
  background-image: url("https://kimaleksander.com/wp-content/uploads/2017/12/soul-surfer-425w.jpg");
  background-position: bottom;
}
@media (min-width: 576px) {
  .ss-hero,
  .ss-signup {
    background-image: url("https://kimaleksander.com/wp-content/uploads/2017/12/soul-surfer-768w.jpg");
  }
}
@media (min-width: 992px) {
  .ss-hero,
  .ss-signup {
    background-image: url("https://kimaleksander.com/wp-content/uploads/2017/12/soul-surfer-1024w.jpg");
    background-position: bottom left;
  }
}
@media (min-width: 1200px) {
  .ss-hero,
  .ss-signup {
    background-image: url("https://kimaleksander.com/wp-content/uploads/2017/12/soul-surfer-1980w.jpg");
  }
}
```

And the images needed to me manually created.

On another site, time was ripe to use CSS `image-set` (with fallbacks). There's a great right up on this on Medium [here](https://medium.freecodecamp.org/a-guide-to-responsive-images-with-ready-to-use-templates-c400bd65c433). Check about halfway down through the article, which is where he gets to image-set.

I used this technique to make the feature images responsive on [this template](https://sakura.abts.io)(yes, It's Jekyll; I'm old!). But again, I had to generate all of the images. Thankfully, theres a great tool, [grunt-responsive-images[https://www.npmjs.com/package/gulp-responsive-images] that takes care of the grunt-work.

So now, I've discovered Gatsby and, IMHO, Gatsby Image is simply the wrong tool for the job of background images in certain cases.

Ideally, a **Gatsby Background Image** component would return a styled div with the background image-sets built with the same magic that Gatsby Image uses.

I'm about to head down the journey, but my chops aren't so hot. Don't hold your breath :smile:
